from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from .models import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import selenium.webdriver.chrome.service as service
import unittest
import socket

class Story6UnitTest(TestCase):
    def test_root_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_submit_url_is_exist(self):
        response = Client().get('/submit/')
        self.assertEqual(response.status_code,302)

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = status(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, apa kabar?', html_response)

    def test_greet_url_is_exist(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('class="col-sm-6"', html_response)

    def test_info_url_is_exist(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('class="col-sm-4"', html_response)

#Functional Test
class Story6FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25) 
        super(Story6FunctionalTest,self).setUp()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://update-kuy.herokuapp.com')
        # find the form element
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('tombol')

        # Fill the form with data
        status.send_keys('coba - coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)

    # def test_layout(self):
    #     selenium = self.selenium
    #     selenium.get('http://update-kuy.herokuapp.com')
    #     table = selenium.find_element_by_id('tabel')
    #     tabel_size = table.size
    #     # greetings = selenium.find_element_by_id('greet')
    #     print(tabel_size)
        # self.assertEqual(table.location, {'x': 0, 'y': })
        # self.assertEqual(greetings.location, {'x': 0, 'y': })
        

    # def test_css(self):
    #     selenium = self.selenium
    #     selenium.get(self.live_server_url)
    #     table = selenium.find_element_by_id("tabel")
    #     greetings = selenium.find_element_by_id("greet")
    #     self.assertEqual(title.value_of_css_property("font-size"), "px")
    #     self.assertEqual(button.value_of_css_property("font-size"), "px")

    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

