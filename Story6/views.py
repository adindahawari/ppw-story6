from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import StatusForm
from .models import StatusModel

# Create your views here.
def about(request):
    response = {}
    return render(request, 'profile.html', response)

def status(request):
    response = {'form': StatusForm, 'stat': StatusModel.objects.all()}
    return render(request, 'statuz.html', response)

def create_stat(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = StatusForm(request.POST)
        # check whether it's valid:
        obj = StatusModel()
        #'status' from models.py
        obj.status = request.POST['status']
        obj.save()
        # redirect to a new URL:
        return HttpResponseRedirect(reverse('status'))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = StatusForm()
    return HttpResponseRedirect(reverse('status'))